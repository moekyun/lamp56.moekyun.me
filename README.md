# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

---

### docker command
* config
```
sudo APP_HOST_PORT=5600 DB_HOST_PORT=35600 /usr/local/bin/docker-compose \
--env-file=./docker/env/local.compose.env \
-f ./docker/docker-compose-http.yml \
-f ./docker/docker-compose-mysql.yml \
-f ./docker/docker-compose-network.yml \
config
```

* up --build -d
```
sudo APP_HOST_PORT=5600 DB_HOST_PORT=35600 /usr/local/bin/docker-compose \
--env-file=./docker/env/local.compose.env \
-f ./docker/docker-compose-http.yml \
-f ./docker/docker-compose-mysql.yml \
-f ./docker/docker-compose-network.yml \
up --build -d
```

* down --rmi all --volumes
```
sudo APP_HOST_PORT=5600 DB_HOST_PORT=35600 /usr/local/bin/docker-compose \
--env-file=./docker/env/local.compose.env \
-f ./docker/docker-compose-http.yml \
-f ./docker/docker-compose-mysql.yml \
-f ./docker/docker-compose-network.yml \
down --rmi all --volumes
```

* attach
```
sudo docker exec -it lamp56.moekyun.me.local.http /bin/bash
sudo docker exec -it lamp56.moekyun.me.local.mysql /bin/bash
```

* 一覧
```
sudo docker volume ls
sudo docker network ls
```

* mysql
```
mysql -uroot -p -hlamp56.moekyun.me.local.mysql
```
---
